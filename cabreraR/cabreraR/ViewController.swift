//
//  ViewController.swift
//  cabreraR
//
//  Created by Ronny Cabrera on 5/6/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tittleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    var dataJson:[TableJson] = []
    
    @IBAction func NextButtonPressed(_ sender: Any) {
        
        
        
        let urlString = "https://api.myjson.com/bins/182sje";
        
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { (data,response, error) in
            
            guard let data = data else{
                print("Error NO data")
                return
            }
            print(data)
            guard let jsonData = try? JSONDecoder().decode(JsonInfo.self, from: data) else {
                print("Error decoding Json")
                return
            }
            DispatchQueue.main.async {
                self.dataJson = jsonData.data
                self.performSegue(withIdentifier: "toTableViewSegue", sender: self)
            }
        }
        task.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier ==  "toTableViewSegue"){
            let destination = segue.destination as! TableViewController
            destination.Labels = self.dataJson
            let destination1 = segue.destination as! TableViewController
            destination.Labels = self.dataJson
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let urlString = "https://api.myjson.com/bins/72936"
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!){ (data, response, error)
            in
            guard let data = data else{
                print("Error NO data")
                return
            }
            guard let jsonData = try? JSONDecoder().decode(Json.self, from: data) else{
                print("Error decoding json")
                return
            }
            
            DispatchQueue.main.async {
                self.tittleLabel.text = "\(jsonData.viewTitle)"
                self.dateLabel.text = "\(jsonData.date)"
                self.nameTextField.text = "\(jsonData.nextLink)"
            }
        }
        task.resume()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

