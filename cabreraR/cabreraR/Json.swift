//
//  Json.swift
//  cabreraR
//
//  Created by Ronny Cabrera on 6/5/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import Foundation

struct JsonInfo: Decodable {
    let data:[TableJson]
}

struct Json: Decodable {
    let viewTitle: String
    let date: String
    let nextLink: String
}

struct TableJson: Decodable {
    let label: String
    let value: Int
}
