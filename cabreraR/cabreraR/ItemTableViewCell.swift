//
//  ItemTableViewCell.swift
//  cabreraR
//
//  Created by Ronny Cabrera on 6/6/18.
//  Copyright © 2018 Ronny Cabrera. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var labelLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func fillDat(label: TableJson) {
        labelLabel.text = label.label
        valueLabel.text = "\(label.value)"
    }
}
